#ifndef RUCH_HH
#define RUCH_HH
#include "Plansza.hh"
#include <vector>

using namespace std;


class Ruch
{
private:
  vector <Pole> ruchy;
public:
  void wyswietl_wspolrzedne();
  void odloz(Pole dokladane); //odklada na listę kolejny ruch 
  void usun();//usuwa ostatnie pole
  void pobierz(int i);//pobiera i-te pole
  int dlugosc_ruchu();//zwraca ilosc pol na stosie
  Pole pomiedzy(); //zwraca pole znajdujące się pomiędzy 1 a drugim na kolejce
  int odleglosc();// zwraca odleglosc, na jaka rusza sie pionek
  bool ukos();//true, gdy ruch wykonywany jest po ukosie
  void odwracanie_wspolrzednych();// odwraca np. A8 na G1 ;)
  bool czy_zwykly_ruch_damy(Plansza &uklad);//zwraca 1 gdy jest to zwykly ruch damy
  bool ruch_damki(Plansza &uklad, int gracz); // ...
  bool twoj_pionek(Plansza uklad,int gracz); // zwroci 1, jesli Pole, z którego gracz chce się ruszyć należy do niego
  bool zwykly_ruch(Plansza& uklad,int gracz); // zwroci 1 gdy bedzie OK, wykona ruch zwyklymi pionkami
  bool wczytaj_ruch(); //wczytuje wspolrzedne z terminala
  bool bicie_pojedyncze(Plansza &uklad,int gracz); //zwroci 1 gdy bedzie OK, wykonuje pojedyncze bicie zwyklych
  bool mozliwe_bicie_pionka(Plansza &uklad, int gracz); //roznica pomiedzy biciem pojedynczym a tym jest taka, ze w tym przypadku bicie jest z przedostatniego na ostatnie pole na wektorze
  bool bicie(Plansza &uklad,int gracz); // wykonuje bicie wielokrotne zwyklych pionkow, jesli takie jest zadane
  bool bicie_pojedyncze_damki(Plansza &uklad, int gracz); // jak nazwa wskazuje
  bool bicie_damki(Plansza &uklad, int gracz); // ewentualne wielokrotne bicie damy
  bool mozliwe_bicie_damy(Plansza &uklad, int gracz); //tak jak w przypadku pionka
  bool runda(Plansza&, int); //cala runda jednego gracza
  bool runda_komputera(Plansza&, int);
};
int interpretuj(char pole); //zamiana liter na cyfry A=0 B=1 itd
void zapowiedz_gracza(int gracz); //wyswietlana informacja o ruchu w terminalu


#endif
