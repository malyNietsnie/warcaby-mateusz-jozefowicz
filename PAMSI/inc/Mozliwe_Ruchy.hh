#ifndef MOZLIWE_RUCHY_HH
#define MOZLIWE_RUCHY_HH
#include "Ruch.hh"

class Mozliwe_Ruchy
{
private:
  vector <Ruch> mozliwosci; //przechowuje mozliwe ruchy w danej rundzie danego gracza
public:
  Ruch pobierz_mozliwosc(int i) //zwraca konkretny element 
  {return mozliwosci[i];}
  Ruch *masz=NULL;
  int dlugosc_mozliwosci()
  {return mozliwosci.size();} //zwraca dlugosc wektora mozliwosci
  void wczytaj_mozliwosci(Plansza uklad, int gracz); //wczytuje wszystkie mozliwe ruchy w danym momencie
  void wszystkie_bicia(Plansza,int); //wczytuje wszystkie bicia
  void wszystkie_zwykle_ruchy(Plansza,int); //wczytuje wszystkie zwykle
  void ruchy_pionka(Plansza jakas, Pole jakies);// wczytuje ...
  void ruchy_damy(Plansza,Pole);
  void bicia_zwyklego(Ruch zdrowy,Plansza stara,Pole,int);
  void bicia_damy(Ruch,Plansza,Pole,int);
  void wyswietlic();//wyswietla mozliwe ruchy.
};
#endif
