#ifndef PIONEK_HH
#define PIONEK_HH

#define PUSTE 0
#define BIALY 1
#define CZARNY 2

#define ZWYKLY 0
#define DAMA 1

class Pionek {
  int czyj=0; //moze byc niczyje (puste), bialy albo czarny, domyslnie niech będzie puste
  int typ=0; //moze byc zwykly albo byc damą 
public:
  void zmien_pionek(int a, int b) // zmienia pionek na inny, np. zwykly na damę
  {czyj=a; typ=b;}
  int czyj_pion() const// zwraca informacje, czyi pion znajduje sie  na polu lub czy jest puste
  {return czyj;}
   int jaki_typ() const// zwraca info o tym, jaki ma typ
  {return typ;}
  bool przeciwnik(int gracz)const; //true, gdy pionek nalezy do przeciwnika gracza
};
  
#endif
