#ifndef DRZEWO_HH
#define DRZEWO_HH
#include "Mozliwe_Ruchy.hh"
#include <algorithm>

class Wezel
{
private:
  Plansza stan;
  int gracz;

public:
  Wezel* poprzedni;
  vector<Wezel*> kolejne; //unique pointer
  void wyswietl_cos()
  {cout << stan;};
  Ruch optymalny();
  void wyzeruj(){poprzedni=NULL;}
  void zmien_plansze(Plansza nowa){stan=nowa;};
  void zmien_gracza(int nowy){gracz=nowy;};
  Plansza stan_planszy(){return stan;}
  int jaki_gracz(){return gracz;}
  void pietro_nizej(int);
  int roznica();
  int minmax();
};


#endif
