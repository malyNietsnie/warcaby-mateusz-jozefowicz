#ifndef PLANSZA_HH
#define PLANSZA_HH

#include "Pionek.hh"
#include "Pole.hh"
#include <iostream>
#include<cmath>

#define DL_PLANSZY 8
using namespace std;

class Plansza {
  Pionek tab_pol[DL_PLANSZY][DL_PLANSZY];
public:
  Plansza(); //inicjuje początkowy układ planszy
  bool poloz(Pole miejsce, Pionek figura); // kladzie w wybrane miejsce pionek (rowniez pole puste)
  bool czy_puste(Pole miejsce); //sprawdza, czy pole jest puste
  Pionek co_tu_stoi(Pole miejsce)const // zwraca pionek, ktory stoi w danym miejscu na planszy
  { return tab_pol[miejsce.pobierz(0)][miejsce.pobierz(1)];}
 friend ostream & operator <<(ostream &wyjscie, const Plansza &uklad); // wyswietlanie planszy
  void obrot(); //obracanie planszy o 180 stopni
  bool czy_jest_bicie(int) const; //sprawdza, czy na planszy jest bicie
  bool zabierz(Pole miejsce); // zabiera pionek z planszy
  void pion_na_dame(int); //zamienia pionki na dame, gdy stoja obok krawedzi przeciwnika
  bool czy_wygrana(); //konczy program i wyswietla komunikat, gdy jeden gracz wygra
  int ilosc_pionkow_gracza(int gracz);
};
bool czy_na_planszy(Pole miejsce); // sprawdza czy takie miejsce wgl znajduje sie na planszy
bool czy_dozwolone(Pole miejsce); // sprawdza czy takie miejsce jest dozwolone (co drugie pole musi byc wolne
bool czy_pionek(Pionek zagadkowy); // sprawdza, czy pionek ma poprawne wartosci

#endif
