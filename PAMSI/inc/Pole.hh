#ifndef POLE_HH
#define POLE_HH

#include <iostream>

using namespace std;

class Pole
{
private:
  int wspolrzedne[2];
public:
  Pole(int x, int y) // konstruktor jawny
  {wspolrzedne[0]=x; wspolrzedne[1]=y;}
  int pobierz(int wspolrzedna) //zwraca wartość wybranej wspolrzednej
  {if (wspolrzedna==0)      return wspolrzedne[0]; else return wspolrzedne[1];}
  void zmien(int x, int y) // zmienia wartosci pola
  {wspolrzedne[0]=x; wspolrzedne[1]=y;}
  void dodaj(int x, int y)
  {wspolrzedne[0]+=x; wspolrzedne[1]+=y;}
  void kontrast()//odwraca wspolrzedne o 180 stopni wzgledem srodka tablicy
  {wspolrzedne[0]=7-wspolrzedne[0];wspolrzedne[1]=7-wspolrzedne[1];}
};



#endif
