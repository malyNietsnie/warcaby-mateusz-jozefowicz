#include "Algorytm.hh"

Ruch Algorytm::wylosuj_ruch(Mozliwe_Ruchy wybieralnia)const
{
  srand(time(NULL));
  int zakres=wybieralnia.dlugosc_mozliwosci();
  int wylosowana;
  wylosowana=rand()%zakres;
  Ruch dowolny=wybieralnia.pobierz_mozliwosc(wylosowana);
  return dowolny;
}
