#include "Mozliwe_Ruchy.hh"
void Mozliwe_Ruchy::wczytaj_mozliwosci(Plansza uklad, int gracz)
{
  if(uklad.czy_jest_bicie(gracz))//jesli jest bicie, sprawdzamy mozliwe bicia, jesli nie ma, sprawdzamy mozliwe zwykle ruchy
    {
      wszystkie_bicia(uklad, gracz);
    }   
  else//jesli uklad nie ma bicia
    {
      wszystkie_zwykle_ruchy(uklad, gracz);
    }
	  
	  
}
void Mozliwe_Ruchy::wszystkie_zwykle_ruchy(Plansza uklad, int gracz)
{
 for(int i=0;i<DL_PLANSZY;i++)
   for(int j=0;j<DL_PLANSZY;j++)
     {
       Pole startowe(j,i);
       if(uklad.co_tu_stoi(startowe).czyj_pion()==gracz)
	 {
	   if(uklad.co_tu_stoi(startowe).jaki_typ()==ZWYKLY)
	     {
	       
	       ruchy_pionka(uklad,startowe);
	     }
	   else
	     ruchy_damy(uklad,startowe);
	 }
	}
} 
void Mozliwe_Ruchy::ruchy_pionka(Plansza uklad, Pole miejsce)
{
  Ruch pom;
  pom.odloz(miejsce);
  miejsce.dodaj(-1,-1);//najpierw po skosie w lewo
  if(czy_na_planszy(miejsce))
    if(uklad.czy_puste(miejsce))
      {
	pom.odloz(miejsce);
	mozliwosci.push_back(pom);
	pom.usun();
      }
  miejsce.dodaj(2,0);//przesuwamy sie obok
  if(czy_na_planszy(miejsce))
    if(uklad.czy_puste(miejsce))
      {
	pom.odloz(miejsce);
	mozliwosci.push_back(pom);
      }
}
void Mozliwe_Ruchy::ruchy_damy(Plansza uklad, Pole miejsce)
{
  Ruch pom;
  Pole start=miejsce;
  pom.odloz(miejsce);
  miejsce.dodaj(-1,-1);//najpierw po skosie w gorny lewy
  while(czy_na_planszy(miejsce)&&uklad.czy_puste(miejsce)) //gdy miejsce obok bedzie znajdowac sie na planszy oraz bedzie puste
    {
      pom.odloz(miejsce);
      mozliwosci.push_back(pom);
      pom.usun();
      miejsce.dodaj(-1,-1);
    }
  miejsce=start;//wracamy na pozycje startowa
   miejsce.dodaj(1,-1);//gorny prawy
  while(czy_na_planszy(miejsce)&&uklad.czy_puste(miejsce)) //gdy miejsce obok bedzie znajdowac sie na planszy oraz bedzie puste
    {
      pom.odloz(miejsce);
      mozliwosci.push_back(pom);
      pom.usun();
      miejsce.dodaj(1,-1);
    }
  miejsce=start;
  miejsce.dodaj(-1,1);//dolny lewy
  while(czy_na_planszy(miejsce)&&uklad.czy_puste(miejsce)) //gdy miejsce obok bedzie znajdowac sie na planszy oraz bedzie puste
    {
      pom.odloz(miejsce);
      mozliwosci.push_back(pom);
      pom.usun();
      miejsce.dodaj(-1,1);
    }
  miejsce=start;
  miejsce.dodaj(1,1);//dolny prawy
  while(czy_na_planszy(miejsce)&&uklad.czy_puste(miejsce)) //gdy miejsce obok bedzie znajdowac sie na planszy oraz bedzie puste
    {
      pom.odloz(miejsce);
      mozliwosci.push_back(pom);
      pom.usun();
      miejsce.dodaj(1,1);
    }
}
void Mozliwe_Ruchy::wszystkie_bicia(Plansza uklad, int gracz)
{
 for(int i=0;i<DL_PLANSZY;i++)
      for(int j=0;j<DL_PLANSZY;j++)
	{
	  Pole startowe(j,i);
	  Ruch wektorek;
	  if(uklad.co_tu_stoi(startowe).czyj_pion()==gracz)
	    {
	      if(uklad.co_tu_stoi(startowe).jaki_typ()==ZWYKLY)
		{
		  wektorek.odloz(startowe);
		  bicia_zwyklego(wektorek,uklad,startowe,gracz);
		}
	      else
		{
		  wektorek.odloz(startowe);
		  bicia_damy(wektorek,uklad,startowe,gracz);
		}
	    }
	}
}
void Mozliwe_Ruchy::bicia_zwyklego(Ruch pom,Plansza uklad, Pole miejsce,int gracz)
{
  int czy_bylo_bicie=0;
  Pole start=miejsce;
  /**tutaj musimy sprawdzic niestety wszystkie 4 opcje**/
  miejsce.dodaj(-2,-2);
  if(czy_na_planszy(miejsce)&&uklad.czy_puste(miejsce))//jesli miejsce o dwa pola dalej po ukosie jest puste (i istnieje)
    {
      miejsce.dodaj(1,1);//wracamy jedno pole w tyl i sprawdzamy, czy stoi na nim przeciwnik
      if(uklad.co_tu_stoi(miejsce).przeciwnik(gracz))//jesli stoi
	{
	  czy_bylo_bicie++;
	  miejsce.dodaj(-1,-1);//przechodzimy znowu na puste pole
	  pom.odloz(miejsce); //odkladamy miejsce, na które jest bicie
	  Plansza nowa=uklad;
	  //kopiujemy uklad planszy
	  if(pom.mozliwe_bicie_pionka(nowa,gracz)) //wykonujemy ruch na skopiowanej tablicy
	    bicia_zwyklego(pom,nowa,miejsce,gracz); //rekurencja.
	  else
	    cout << "nie dziala";
	  pom.usun();//usuwamy ostatni klocek ;)
	}
    } 
  miejsce=start;
  miejsce.dodaj(2,-2);
  if(czy_na_planszy(miejsce)&&uklad.czy_puste(miejsce))//jesli miejsce o dwa pola dalej po ukosie jest puste (i istnieje)
    {
      miejsce.dodaj(-1,1);//wracamy jedno pole w tyl i sprawdzamy, czy stoi na nim przeciwnik
      if(uklad.co_tu_stoi(miejsce).przeciwnik(gracz))//jesli stoi
	{
	  czy_bylo_bicie++;
	  miejsce.dodaj(1,-1);//przechodzimy znowu na puste pole
	  pom.odloz(miejsce); //odkladamy miejsce, na które jest bicie
	  Plansza nowa2=uklad; //kopiujemy uklad planszy
	  if(pom.mozliwe_bicie_pionka(nowa2,gracz)) //wykonujemy ruch na skopiowanej tablicy
	    bicia_zwyklego(pom,nowa2,miejsce,gracz); //rekurencja.
	  else
	    cout << "nie dziala";
	  pom.usun();
	}
    }
  miejsce=start;
  miejsce.dodaj(2,2);
  if(czy_na_planszy(miejsce)&&uklad.czy_puste(miejsce))//jesli miejsce o dwa pola dalej po ukosie jest puste (i istnieje)
    {
      miejsce.dodaj(-1,-1);//wracamy jedno pole w tyl i sprawdzamy, czy stoi na nim przeciwnik
      if(uklad.co_tu_stoi(miejsce).przeciwnik(gracz))//jesli stoi
	{
	  czy_bylo_bicie++;
	  miejsce.dodaj(1,1);//przechodzimy znowu na puste pole
	  pom.odloz(miejsce); //odkladamy miejsce, na które jest bicie
	  Plansza nowa3=uklad; //kopiujemy uklad planszy
	   if(pom.mozliwe_bicie_pionka(nowa3,gracz)) //wykonujemy ruch na skopiowanej tablicy
	    bicia_zwyklego(pom,nowa3,miejsce,gracz); //rekurencja.
	  else
	    cout << "nie dziala";
	  pom.usun();
	}
    }
  miejsce=start;
  miejsce.dodaj(-2,2);
  if(czy_na_planszy(miejsce)&&uklad.czy_puste(miejsce))//jesli miejsce o dwa pola dalej po ukosie jest puste (i istnieje)
    {
      miejsce.dodaj(1,-1);//wracamy jedno pole w tyl i sprawdzamy, czy stoi na nim przeciwnik
      if(uklad.co_tu_stoi(miejsce).przeciwnik(gracz))//jesli stoi
	{
	  czy_bylo_bicie++;
	  miejsce.dodaj(-1,1);//przechodzimy znowu na puste pole
	  pom.odloz(miejsce); //odkladamy miejsce, na które jest bicie
	  Plansza nowa4=uklad; //kopiujemy uklad planszy
	   if(pom.mozliwe_bicie_pionka(nowa4,gracz)) //wykonujemy ruch na skopiowanej tablicy
	    bicia_zwyklego(pom,nowa4,miejsce,gracz); //rekurencja.
	  else
	    cout << "nie dziala";
	  pom.usun();
	}
    }
    if(pom.dlugosc_ruchu()>1)//jesli jestesmy na koncu lancucha
      {
      mozliwosci.push_back(pom);
      }
}
void Mozliwe_Ruchy::wyswietlic()
{
  for(unsigned int i=0; i<mozliwosci.size(); i++)
    {
      cout << "Mozliwosc nr: " << i << endl;
      mozliwosci[i].wyswietl_wspolrzedne();
    }
    }
void Mozliwe_Ruchy::bicia_damy(Ruch pom,Plansza uklad, Pole miejsce,int gracz)
{
  Pole start=miejsce;
  int x,y; //wspolrzedne przesuwania
  x=-1;y=-1;
  miejsce.dodaj(x,y);
  while(czy_na_planszy(miejsce)&&uklad.czy_puste(miejsce))
    miejsce.dodaj(x,y); //przesuwamy po ukosie az natrafimy na cos innego niz puste pole
  if(uklad.co_tu_stoi(miejsce).przeciwnik(gracz))
    {
      miejsce.dodaj(x,y);
      while(czy_na_planszy(miejsce)&&uklad.czy_puste(miejsce))
	{
	
	  pom.odloz(miejsce); //odkladamy miejsce, na które jest bicie
	  Plansza nowa =uklad; //kopiujemy uklad planszy
	   if(pom.mozliwe_bicie_damy(nowa,gracz)) //wykonujemy ruch na skopiowanej tablicy
	    bicia_damy(pom,nowa,miejsce,gracz); //rekurencja.
	  else
	    cout << "nie dziala";
	  pom.usun();
	  miejsce.dodaj(x,y);
	}
    }
  miejsce=start;
  x=-1;y=1;
  miejsce.dodaj(x,y);
  while(czy_na_planszy(miejsce)&&uklad.czy_puste(miejsce))
    miejsce.dodaj(x,y); //przesuwamy po ukosie az natrafimy na cos innego niz puste pole
  if(uklad.co_tu_stoi(miejsce).przeciwnik(gracz))
    {
      miejsce.dodaj(x,y);
      while(czy_na_planszy(miejsce)&&uklad.czy_puste(miejsce))
	{
	  pom.odloz(miejsce); //odkladamy miejsce, na które jest bicie
	  Plansza nowa =uklad; //kopiujemy uklad planszy
	   if(pom.mozliwe_bicie_damy(nowa,gracz)) //wykonujemy ruch na skopiowanej tablicy
	    bicia_damy(pom,nowa,miejsce,gracz); //rekurencja.
	  else
	    cout << "nie dziala";
	  pom.usun();
	  miejsce.dodaj(x,y);
	}
    }
  miejsce=start;
    x=1;y=1;
    miejsce.dodaj(x,y);
  while(czy_na_planszy(miejsce)&&uklad.czy_puste(miejsce))
    miejsce.dodaj(x,y); //przesuwamy po ukosie az natrafimy na cos innego niz puste pole
  if(uklad.co_tu_stoi(miejsce).przeciwnik(gracz))
    {
      miejsce.dodaj(x,y);
      while(czy_na_planszy(miejsce)&&uklad.czy_puste(miejsce))
	{
	  pom.odloz(miejsce); //odkladamy miejsce, na które jest bicie
	  Plansza nowa =uklad; //kopiujemy uklad planszy
	   if(pom.mozliwe_bicie_damy(nowa,gracz)) //wykonujemy ruch na skopiowanej tablicy
	    bicia_damy(pom,nowa,miejsce,gracz); //rekurencja.
	  else
	    cout << "nie dziala";
	  pom.usun();
	  miejsce.dodaj(x,y);
	}
    }
  miejsce=start;
    x=1;y=-1;
    miejsce.dodaj(x,y);
  while(czy_na_planszy(miejsce)&&uklad.czy_puste(miejsce))
    miejsce.dodaj(x,y); //przesuwamy po ukosie az natrafimy na cos innego niz puste pole
  if(uklad.co_tu_stoi(miejsce).przeciwnik(gracz))
    {
      miejsce.dodaj(x,y);
      while(czy_na_planszy(miejsce)&&uklad.czy_puste(miejsce))
	{
	  pom.odloz(miejsce); //odkladamy miejsce, na które jest bicie
	  Plansza nowa =uklad; //kopiujemy uklad planszy
	   if(pom.mozliwe_bicie_damy(nowa,gracz)) //wykonujemy ruch na skopiowanej tablicy
	    bicia_damy(pom,nowa,miejsce,gracz); //rekurencja.
	  else
	    cout << "nie dziala";
	  pom.usun();
	  miejsce.dodaj(x,y);
	}
    }
  if(pom.dlugosc_ruchu()>1)//jesli jestesmy na koncu lancucha
    {
      mozliwosci.push_back(pom);
    }
}
