#include "Drzewo.hh"

void Wezel::pietro_nizej(int a)
{
  a--;
  int wrog;
  if (gracz==BIALY)
    wrog=CZARNY;
  else
    wrog=BIALY;
  Ruch odnoga;
  Mozliwe_Ruchy duszki;
  Plansza pom;
   duszki.wczytaj_mozliwosci(stan,gracz);
  for(int i=0;i<duszki.dlugosc_mozliwosci();i++)
    {
      odnoga=duszki.pobierz_mozliwosc(i);
      pom=stan;
      odnoga.runda_komputera(pom,gracz);
      Wezel *predator=new Wezel;
      predator->poprzedni=this;
      pom.obrot();
      predator->zmien_plansze(pom);
      predator->zmien_gracza(wrog);
      kolejne.push_back(predator);
      if(a>0)
	  predator->pietro_nizej(a);
    }
}
int Wezel::roznica()
{
  return (stan.ilosc_pionkow_gracza(BIALY)-stan.ilosc_pionkow_gracza(CZARNY));
}
int Wezel::minmax()
{
  if(kolejne.size()<1)
    {
      return roznica();
    }
  else
    {
      int nowa;
      int aktualna=kolejne[0]->minmax();
      for(unsigned int i=1;i<kolejne.size();i++)
	{
	  nowa=kolejne[i]->minmax();
	  {
	    if(gracz==BIALY)
	      aktualna=max(nowa,aktualna);
	    else
	      aktualna=min(nowa,aktualna);
	  }
	}
      return aktualna;
    }
}
Ruch Wezel::optymalny()
{
  unsigned int numer_opcji=0;
  int aktualna=kolejne[0]->minmax();
  int nowa;
  unsigned int i;
  Mozliwe_Ruchy niby;
  niby.wczytaj_mozliwosci(stan,gracz);
  for(i=1; i<kolejne.size();i++)
    {
      nowa=kolejne[i]->minmax();
      aktualna=max(nowa,aktualna);
      if(nowa==aktualna)
	numer_opcji=i;
    }
  return niby.pobierz_mozliwosc(numer_opcji);
}
