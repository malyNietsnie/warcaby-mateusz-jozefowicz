#include "Ruch.hh"
void Ruch::odloz(Pole nowe)
{
   ruchy.push_back(nowe);
}
void Ruch::usun()
{
  ruchy.pop_back();
}
int Ruch::dlugosc_ruchu()
{
  return ruchy.size();
}

void Ruch::odwracanie_wspolrzednych()
{
  for(unsigned int i=0; i<ruchy.size(); i++)
       ruchy[i].kontrast();
}
void Ruch::wyswietl_wspolrzedne()
{
  for(unsigned int i=0; i<ruchy.size(); i++)
    {
      cout << "Pierwsza wspolrzedna pola" << i << " = " << ruchy[i].pobierz(0) << " Druga = " << ruchy[i].pobierz(1) << endl;
    }
    }
int interpretuj(char pole)
{ int liczba;
  switch(pole)
    {
    case 'A':
      liczba=0;
      break;
    case 'B':
      liczba=1;
      break;
    case 'C':
      liczba=2;
      break;
    case 'D':
      liczba=3;
      break;
    case 'E':
      liczba=4;
      break;
    case 'F':
      liczba=5;
      break;
    case 'G':
      liczba=6;
      break;
    case 'H':
      liczba=7;
      break;
    default:
      cout << "Nieznana liczba" << endl;
      return 10;
      break;
    }
  return liczba;
}
bool Ruch::wczytaj_ruch()
{
  int x,y;
  char napis[4];
  cout << "Wykonaj ruch wpisując kolejne współrzędne oddzielone spacją. Zakończywszy ruch nacisnij kropkę." <<  endl;
  Pole nowe(0,0);
  while(napis[2]!='.')
    {
      cin >> napis;
      if(napis[0]=='.')
	break;
      x=interpretuj(napis[0]);
      if(x==10)
	{
	  cout << "Niedozwolona litera" << endl;
	  return false;
	}
      y=(int)napis[1]-49;
      if(y>=DL_PLANSZY|| y<0)
	{
	  cout << "Niedozwolona liczba" << endl;
	  return false;
	}
      nowe.zmien(x,y);
      ruchy.push_back(nowe);
    }
  if(ruchy.size()>=2)
    return true;
  else
    {
      cout << "Zbyt mało współrzędnych" << endl;
      return false;
    }
}
Pole Ruch::pomiedzy()
{
  int x,y;            
  x=(ruchy[1].pobierz(0)+ruchy[0].pobierz(0))/2;
  y=(ruchy[1].pobierz(1)+ruchy[0].pobierz(1))/2;
  Pole srodek(x,y);
  return srodek;
}
int Ruch::odleglosc()
{
  int x;
  x=abs(ruchy[1].pobierz(0)-ruchy[0].pobierz(0));
  return x;
}
bool Ruch::ukos()
{
  int x,y;
  x=abs(ruchy[1].pobierz(0)-ruchy[0].pobierz(0));
  y=abs(ruchy[1].pobierz(1)-ruchy[0].pobierz(1));
  if(x/y==1 && x%y==0)
    return true;
  else
    {
      cout << x << y << "nie stoją na ukos" <<endl ;
    return false;
    }
}
bool Ruch::twoj_pionek(Plansza uklad,int gracz)
{
  if(uklad.co_tu_stoi(ruchy[0]).czyj_pion()==gracz)
    return true;
  else
    {
      cout <<" To nie twój pionek!" << endl;
      return false;
    }
}
bool Ruch::zwykly_ruch(Plansza &uklad,int gracz)
{
  //sprawdzamy, czy na planszy jest bicie
  if(uklad.czy_jest_bicie(gracz)) //zwraca 1, jesli jest
    {
      cout << "Bicie ma pierwszenstwo" << endl;
      return 0;
    }
  //sprawdzamy, czy ruch który chcemy wykonać jest na ukos do przodu
  if(ukos())
    if(odleglosc()==1)
      if(ruchy[0].pobierz(1)>ruchy[1].pobierz(1))
	  if(uklad.poloz(ruchy[1],uklad.co_tu_stoi(ruchy[0])))
	    if(uklad.zabierz(ruchy[0]))
	      return 1;   
  return 0;
}
//bicie pojedyncze
bool Ruch::bicie_pojedyncze(Plansza &uklad, int gracz)
{
  if(ukos())// jesli pole startowe i docelowe sa do siebie po ukosie
    if(odleglosc()==2) // a ich odleglosc od siebie rowna jest dwa
      if(uklad.co_tu_stoi(pomiedzy()).przeciwnik(gracz))//jesli pomiedzy jest przeciwnik
	if(uklad.co_tu_stoi(ruchy[1]).czyj_pion()==PUSTE)//a za nim jest puste pole
	  if(uklad.poloz(ruchy[1],uklad.co_tu_stoi(ruchy[0]))) //poloz pionek na puste pole
	    if(uklad.zabierz(pomiedzy())) //zabierz pion przeciwnika
	      if(uklad.zabierz(ruchy[0])) //zabierz jego samego
		return 1;
  return 0;
}
bool Ruch::mozliwe_bicie_pionka(Plansza &uklad, int gracz)
{
  int ostatni=dlugosc_ruchu()-1;
  int x,y;
  x=(ruchy[ostatni].pobierz(0)+ruchy[ostatni-1].pobierz(0))/2;
  y=(ruchy[ostatni].pobierz(1)+ruchy[ostatni-1].pobierz(1))/2;
  Pole srodek(x,y);
  if(uklad.poloz(ruchy[ostatni],uklad.co_tu_stoi(ruchy[ostatni-1])))
    if(uklad.zabierz(srodek))
      if(uklad.zabierz(ruchy[ostatni-1]))
	return 1;
	{
	  cout << "mozliwe pojedyncze bicie zwyklego pionka szwankuje!" << endl;
	  return 0;
	}
}
bool Ruch::bicie(Plansza &uklad,int gracz)
{
  if(bicie_pojedyncze(uklad,gracz))
    {
      ruchy.erase(ruchy.begin());
      while(ruchy.size()>1) //tak dlugo, az skonczy sie lista polecen albo blad;
	{
	  if(0==bicie_pojedyncze(uklad,gracz))
	    break;
	}
      return true;
    }
  else
    return false;
}

bool Ruch::czy_zwykly_ruch_damy(Plansza &uklad)
{
  int x,y; //wektory
  x=ruchy[0].pobierz(0);
  y=ruchy[0].pobierz(1);
  if(ukos())
    {
      for(int i=0;i<odleglosc();i++)
	{
	  x+=(ruchy[1].pobierz(0)-ruchy[0].pobierz(0))/odleglosc();
	  y+=(ruchy[1].pobierz(1)-ruchy[0].pobierz(1))/odleglosc();
	  Pole miejsce(x,y);
	  if(uklad.co_tu_stoi(miejsce).czyj_pion()!=PUSTE)
	    return 0;
	}
      return 1;
    }
  return 0;
}
bool Ruch::ruch_damki(Plansza &uklad, int aktualny_gracz)
{
   if(uklad.czy_jest_bicie(aktualny_gracz)) //zwraca 1, jesli jest
    {
      cout << "Bicie ma pierwszenstwo" << endl;
      return 0;
    } if(uklad.czy_jest_bicie(aktualny_gracz)) //zwraca 1, jesli jest
    {
      cout << "Bicie ma pierwszenstwo" << endl;
      return 0;
    }
  //wiemy że to damka
  //że stoją na ukos
  //że pomiedzy nimi są puste pola
  //więc...
  if(uklad.poloz(ruchy[1],uklad.co_tu_stoi(ruchy[0])))
    if(uklad.zabierz(ruchy[0]))
      return 1;
  return 0;
}
bool Ruch::bicie_pojedyncze_damki(Plansza &uklad,int aktualny_gracz)
{
  int x,y; //wspolrzedne
  x=ruchy[0].pobierz(0);
  y=ruchy[0].pobierz(1);
  if(ukos())
    {
      for(int i=1;i<odleglosc();i++)
	{
	  x+=(ruchy[1].pobierz(0)-ruchy[0].pobierz(0))/odleglosc();
	  y+=(ruchy[1].pobierz(1)-ruchy[0].pobierz(1))/odleglosc();
	  Pole miejsce(x,y);
	  if(!uklad.co_tu_stoi(miejsce).czyj_pion()==PUSTE)//jesli pole nie jest puste
	    { 
	      if(uklad.co_tu_stoi(miejsce).przeciwnik(aktualny_gracz))//ale nalezy do przeciwnego gracza
		{
		  Pole zbity(x,y);
		  for(;i<odleglosc();i++)
		    {
		      x+=(ruchy[1].pobierz(0)-ruchy[0].pobierz(0))/odleglosc();
		      y+=(ruchy[1].pobierz(1)-ruchy[0].pobierz(1))/odleglosc();
		      Pole zbity(x,y);
		      if(!uklad.co_tu_stoi(zbity).czyj_pion()==PUSTE)
			return 0;
		    }
		  if(uklad.poloz(ruchy[1],uklad.co_tu_stoi(ruchy[0]))) //poloz pionek na puste pole
		    if(uklad.zabierz(zbity)) //zabierz pion przeciwnika
		      if(uklad.zabierz(ruchy[0])) //zabierz jego samego
			return 1;
		}
	    }
	}
    }
  return 0;
}bool Ruch::mozliwe_bicie_damy(Plansza &uklad,int aktualny_gracz)
{
  int x,y; //wspolrzedne
  int ostatni=dlugosc_ruchu()-1;
  x=ruchy[ostatni-1].pobierz(0);
  y=ruchy[ostatni-1].pobierz(1);

  for(int i=1;i<abs(ruchy[ostatni-1].pobierz(0)-ruchy[ostatni].pobierz(0));i++)
	{
	  x+=(ruchy[ostatni].pobierz(0)-ruchy[ostatni-1].pobierz(0))/abs(ruchy[ostatni].pobierz(0)-ruchy[ostatni-1].pobierz(0));
	  y+=(ruchy[ostatni].pobierz(1)-ruchy[ostatni-1].pobierz(1))/abs(ruchy[ostatni].pobierz(1)-ruchy[ostatni-1].pobierz(1));
	  Pole miejsce(x,y);
	  if(!uklad.co_tu_stoi(miejsce).czyj_pion()==PUSTE)//jesli pole nie jest puste
	    { 
	      if(uklad.co_tu_stoi(miejsce).przeciwnik(aktualny_gracz))//ale nalezy do przeciwnego gracza
		{
		  Pole zbity(x,y);
		  for(;i<abs(ruchy[ostatni-1].pobierz(0)-ruchy[ostatni].pobierz(0));i++)
		    {
		       x+=(ruchy[ostatni].pobierz(0)-ruchy[ostatni-1].pobierz(0))/abs(ruchy[ostatni].pobierz(0)-ruchy[ostatni-1].pobierz(0));
	  y+=(ruchy[ostatni].pobierz(1)-ruchy[ostatni-1].pobierz(1))/abs(ruchy[ostatni].pobierz(1)-ruchy[ostatni-1].pobierz(1));
		      Pole zbity(x,y);
		      if(!uklad.co_tu_stoi(zbity).czyj_pion()==PUSTE)
			return 0;
		    }
		  if(uklad.poloz(ruchy[ostatni],uklad.co_tu_stoi(ruchy[ostatni-1]))) //poloz pionek na puste pole
		    if(uklad.zabierz(zbity)) //zabierz pion przeciwnika
		      if(uklad.zabierz(ruchy[ostatni-1])) //zabierz jego samego
			return 1;
		}
	    }
	}

  return 0;
}
bool Ruch::bicie_damki(Plansza &uklad,int gracz)
{
  if(bicie_pojedyncze_damki(uklad,gracz))
    {
      ruchy.erase(ruchy.begin());
      while(ruchy.size()>1) //tak dlugo, az skonczy sie lista polecen albo blad;
	{
	  if(0==bicie_pojedyncze_damki(uklad,gracz))
	    break;
	}
      return true;
    }
  else
    return false;
}
void zapowiedz_gracza(int gracz)
{
  switch(gracz)
    {
    case(CZARNY):
      cout << "Ruch okrągłych pionków" << endl;
      break;
    case(BIALY):
      cout << "Krzyżyki do ataku" << endl;
      break;
    }
}
bool Ruch::runda(Plansza &uklad, int gracz) // test
{
  zapowiedz_gracza(gracz);
  if(wczytaj_ruch()) //wczytujemy ruch
    {
      if(gracz==BIALY) //gracz bialy musi miec odwrocone wspolrzedne
	odwracanie_wspolrzednych();
      
      if (uklad.co_tu_stoi(ruchy[0]).czyj_pion()==gracz) //sprawdzamy czy pionek nalezy do gracza, który wykonuje ruch
	if(uklad.co_tu_stoi(ruchy[0]).jaki_typ()==ZWYKLY)
	  {
	    if(odleglosc()<2)//na podstawie tej funkcji decyduję, czy jest to bicie czy zwykly ruch
	      {
		if(zwykly_ruch(uklad,gracz)) // jesli odleglosc 1 to wykonaj zwykly ruch
		  {
		    uklad.pion_na_dame(gracz); //zamien na dame jesli stanął na polu "zamiany"
		    return true; // zwroc 1 jesli sie uda
		  }
	      }
	    else
	      if(bicie(uklad,gracz))//jesli odleglosc 2 to wykonaj bicie
		{
		  uklad.pion_na_dame(gracz);
		  return true; //zwroc 1 jestli sie udalo
		}
	  }
	else
	  if(czy_zwykly_ruch_damy(uklad))
	    {
	      if(ruch_damki(uklad,gracz))
		  return true;
	    }
	  else
	    {	     
	      if(bicie_damki(uklad,gracz))
		return true;
	    }
    }
  ruchy.clear();
  return false; //jesli cos poszlo nie tak, zwroc false;
}

bool Ruch::runda_komputera(Plansza &uklad, int gracz) // test
{
  if (uklad.co_tu_stoi(ruchy[0]).czyj_pion()==gracz) //sprawdzamy czy pionek nalezy do gracza, który wykonuje ruch
    {
    if(uklad.co_tu_stoi(ruchy[0]).jaki_typ()==ZWYKLY)
      {
	if(odleglosc()<2)//na podstawie tej funkcji decyduję, czy jest to bicie czy zwykly ruch
	  {
	    if(zwykly_ruch(uklad,gracz)) // jesli odleglosc 1 to wykonaj zwykly ruch
	      {
		uklad.pion_na_dame(gracz); //zamien na dame jesli stanął na polu "zamiany"
		return true; // zwroc 1 jesli sie uda
	      }
	  }
	else
	  if(bicie(uklad,gracz))//jesli odleglosc 2 to wykonaj bicie
	    {
	      uklad.pion_na_dame(gracz);
	      return true; //zwroc 1 jestli sie udalo
	    }
      }
    else
      if(czy_zwykly_ruch_damy(uklad))
	{
	  if(ruch_damki(uklad,gracz))
	    return true;
	}
      else
	{	     
	  if(bicie_damki(uklad,gracz))
	    return true;
	}
    }
  cout << "cos poszlo nie tak"; //jesli cos poszlo nie tak, zwroc false;
  return false;
}
