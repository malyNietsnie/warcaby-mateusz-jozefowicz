#include "Bitwa.hh"
#include <unistd.h>
bool dwie_tury(Plansza&partia)
{
  Plansza pom;
  Ruch pierwszy,drugi;
  cout << partia;
  while(!pierwszy.runda(partia,CZARNY))
    cout <<partia;
  if(!partia.czy_wygrana())
    return 0;
  cout << partia;
  pom=partia;
  partia.obrot();
  while(!drugi.runda(partia,BIALY))
    cout << pom;
  partia.obrot();
  return(partia.czy_wygrana());
}
bool dwie_tury_SI(Plansza &partia)
{
  Plansza pom;
  Ruch pierwszy,drugi;
  Wezel startowy;
  cout << partia;
  while(!pierwszy.runda(partia,CZARNY))
    cout <<partia;
  if(!partia.czy_wygrana())
    return 0;
  cout << partia;
  usleep(1000000);
  partia.obrot();
  startowy.wyzeruj();
  startowy.zmien_gracza(BIALY);
  startowy.zmien_plansze(partia);
  startowy.pietro_nizej(5);
  drugi=startowy.optymalny();
  drugi.runda_komputera(partia,BIALY);
  partia.obrot();
  return(partia.czy_wygrana());
}
void gra()
{
   Plansza rozgrywka;
    while(dwie_tury(rozgrywka));
}
void gra_z_SI()
{
  Plansza rozgrywka;
  while(dwie_tury_SI(rozgrywka));
}
