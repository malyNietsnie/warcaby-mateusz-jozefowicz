#include "Plansza.hh"


void Plansza::obrot()
{
  int pom1, pom2;
  Pionek tymczasowa[DL_PLANSZY][DL_PLANSZY];
  for (pom1=0;pom1<DL_PLANSZY;pom1++)
    for(pom2=0;pom2<DL_PLANSZY;pom2++)
      tymczasowa[pom2][pom1]=tab_pol[DL_PLANSZY-pom2-1][DL_PLANSZY-pom1-1];//-1 wynika z faktu, ze tablice iterujemy od 0 do 7, chociaz jej dlugosc jest rowna 8... 
  for (pom1=0;pom1<DL_PLANSZY;pom1++)
    for(pom2=0;pom2<DL_PLANSZY;pom2++)
      tab_pol[pom2][pom1]=tymczasowa[pom2][pom1];
}

bool Plansza::czy_puste(Pole miejsce)
{ 
  if(tab_pol[miejsce.pobierz(0)][miejsce.pobierz(1)].czyj_pion()==0)
    return true;
  else
    return false;
}
//funkcja inincjująca początkowy stan gry

Plansza::Plansza()
{
  int pole_x,pole_y;
  for (pole_y=0;pole_y<3;pole_y++) //dla 1 i 3 rzędu należy ustawić pionki jednego gracza w pozycji -X-X-X-X oraz w 2 rzędzie X-X-X-X-
    if(pole_y==1) // jesli jestesmy w drugim rzędzie
      for(pole_x=0;pole_x<DL_PLANSZY;pole_x++)
	{
	    tab_pol[pole_x][pole_y].zmien_pionek(BIALY,ZWYKLY); //ustaw pionek
	  pole_x++; // po czym pozostaw jedno pole puste (zabronione)
	}
    else // czyli gdy jestesmy w 1 lub 3 rzędzie
      for(pole_x=0;pole_x<DL_PLANSZY;pole_x++)
	{
	  pole_x++; //pozostaw wolne pole (konfiguracja -X-X-X-X)
	  tab_pol[pole_x][pole_y].zmien_pionek(BIALY,ZWYKLY);  //postaw pionek
	}
  for (pole_y=5;pole_y<DL_PLANSZY;pole_y++) //analogicznie dla rzędów 6-8 należących do gracza nr 2 
    if(pole_y==6) // nalezy pamietac, ze to rząd 7!
      for(pole_x=0;pole_x<DL_PLANSZY;pole_x++)
	{
	  pole_x++;
	  tab_pol[pole_x][pole_y].zmien_pionek(CZARNY,ZWYKLY);
      }
  else
    for(pole_x=0;pole_x<DL_PLANSZY;pole_x++)
      {      
	tab_pol[pole_x][pole_y].zmien_pionek(CZARNY,ZWYKLY);
      pole_x++;
      }
}



std::ostream& operator << (std::ostream &wyjscie, const Plansza &uklad)
{
  int pole_x, pole_y;

  wyjscie << endl;
  wyjscie << "  | A | B | C | D | E | F | G | H |" << endl;
  
  for(pole_y=0; pole_y<DL_PLANSZY;pole_y++)
    {
      wyjscie << "--------------------------------------" << endl;
      wyjscie << (pole_y+1)<< " |"  ;
      for(pole_x=0; pole_x<DL_PLANSZY; pole_x++)
	{
	  switch(uklad.tab_pol[pole_x][pole_y].czyj_pion())
				  {
				  case 0:
				    wyjscie << "   |" ;
				    break;
				  case 1:
				     if(uklad.tab_pol[pole_x][pole_y].jaki_typ()==0)
				      wyjscie << " x |"  ;
				    else
				      wyjscie << " X |"  ;
				    break;
				   case 2:
				      if(uklad.tab_pol[pole_x][pole_y].jaki_typ()==0)
				      wyjscie << " o |"  ;
				    else
				      wyjscie << " O |"  ;
				     break;
				  default:
				    wyjscie << "co to";
				    break;
				  }
	}
      wyjscie <<" "<< pole_y+1 << endl;
    }
  wyjscie << "--------------------------------------" << endl;
  wyjscie << "  | A | B | C | D | E | F | G | H |" << endl;
  return wyjscie;
}
//korzystam z zaleznosci, ze gdy suma x+y jest parzysta, to pole niedozwolone

bool czy_dozwolone(Pole miejsce)
{
  int suma;
  suma=miejsce.pobierz(0)+miejsce.pobierz(1);
  if(suma%2==1)
    return true;
  else
    return false;
    }
//po prostu sprawdzamy, czy int zawiera sie w przedziale od 0 do 5 
bool czy_pionek(Pionek pion)
{
  if (pion.czyj_pion()<=CZARNY || pion.czyj_pion()>=PUSTE)
    if(pion.jaki_typ()==0 || pion.jaki_typ()==1)
      return true;
  cout << "to nie pionek" << endl;
  return false;
}
bool czy_na_planszy(Pole miejsce)
{
  if(miejsce.pobierz(0)>=0 && miejsce.pobierz(1)>=0)
    if(miejsce.pobierz(0)<DL_PLANSZY && miejsce.pobierz(1)<DL_PLANSZY)
      return true;
    else
      return false;
  else
    return false;
}
bool Plansza::poloz(Pole miejsce, Pionek figura)
{
  if(czy_pionek(figura))
    if(czy_na_planszy(miejsce))
      if(czy_dozwolone(miejsce))
	if(czy_puste(miejsce))
	  {
	    tab_pol[miejsce.pobierz(0)][miejsce.pobierz(1)]=figura;
	    return true;
	  }
	else
	  {
	    cout << "Pole nie jest puste." << endl;
	    return false;
	  }
      else
	{
	  cout << "Pole jest zabronione!" << endl;
	  return false;
	}
    else
      {
	cout << "Poza planszą." << endl;
	return false;
      }
  else
    {
      cout << "To w ogóle nie jest pionek!!!"<< endl;
      return false;
    }
}

bool Plansza::czy_jest_bicie(int aktualny_gracz)const
{
  int i,j; //wspolrzedne do pętli planszy
  int x,y; //wspolrzedne do odwolywania sie do znaków
  
  for(i=0;i<DL_PLANSZY;i++)
    for(j=0;j<DL_PLANSZY;j++)
      {
	y=i;
	x=j;
	if(tab_pol[x][y].czyj_pion()==aktualny_gracz) //jesli pionek nalezy do gracza
	  {
	    if(tab_pol[x][y].jaki_typ()==DAMA)//jesli mamy dame
	      {
		while(tab_pol[x+1][y+1].czyj_pion()==PUSTE && x+2<DL_PLANSZY && y+2<DL_PLANSZY)//dopoki mamy puste pole
		  {		         
		    x+=1;
		    y+=1;		    
		  }
	      }
	    if(x+2<DL_PLANSZY && y+2<DL_PLANSZY) // nie chcemy odwolywac sie do pola, ktorego nie ma na planszy.
	      if (tab_pol[x+1][y+1].przeciwnik(aktualny_gracz)) // czy obok po skosie jest przeciwnik?
		if (tab_pol[x+2][y+2].czyj_pion()==PUSTE) // czy za nim jest puste pole?
		  return 1;
	    x=j;
	    y=i;
	    if(tab_pol[x][y].jaki_typ()==DAMA)//jesli mamy dame
	      {
		while(tab_pol[x+1][y-1].czyj_pion()==PUSTE && x+2<DL_PLANSZY && y-1>0)//dopoki mamy puste pole
		  {		    
		    x+=1;
		    y-=1;		      
		  }
	      }
	    if(x+2<DL_PLANSZY && y-2>=0)
		if (tab_pol[x+1][y-1].przeciwnik(aktualny_gracz)) //analogicznie w pozostale 3 strony
		  if (tab_pol[x+2][y-2].czyj_pion()==PUSTE)
		    return 1;
	      x=j;
	      y=i;
	      if(tab_pol[x][y].jaki_typ()==DAMA)//jesli mamy dame
		{
		  while(tab_pol[x-1][y+1].czyj_pion()==PUSTE && x-2>0 && y+2<DL_PLANSZY)//dopoki mamy puste pole
		    {   
		      x-=1;
		      y+=1;		
		    }
		}
	      if(x-2>=0 && y+2<DL_PLANSZY)
		if (tab_pol[x-1][y+1].przeciwnik(aktualny_gracz))
		  if (tab_pol[x-2][y+2].czyj_pion()==PUSTE)
		    return 1;
	      x=j;
	      y=i;
	      if(tab_pol[x][y].jaki_typ()==DAMA)//jesli mamy dame
		{
		  while(tab_pol[x-1][y-1].czyj_pion()==PUSTE && x-2>0 && y-2>0)//dopoki mamy puste pole
		    {
		      x-=1;
		      y-=1;
		    }
		}
	      if(x-2>=0 && y-2>=0)
		if (tab_pol[x-1][y-1].przeciwnik(aktualny_gracz))
		  if (tab_pol[x-2][y-2].czyj_pion()==PUSTE)
		    return 1;
	  }
      }
  return 0;
}

bool Plansza::zabierz(Pole miejsce)
{
  Pionek puste;
  puste.zmien_pionek(PUSTE,ZWYKLY); //inicjujemy pole puste
  tab_pol[miejsce.pobierz(0)][miejsce.pobierz(1)]=puste;
  if(tab_pol[miejsce.pobierz(0)][miejsce.pobierz(1)].czyj_pion()==PUSTE) // i odkladamy je na dane miejsce :)
    return true;
  else
    return false;
}
void Plansza::pion_na_dame(int aktualny_gracz)
{
  int i;
  for(i=0;i<DL_PLANSZY;i++)
    {
      if(tab_pol[i][0].czyj_pion()==aktualny_gracz)
	if(tab_pol[i][0].jaki_typ()==ZWYKLY)
	  {
	    Pionek damka;
	    damka.zmien_pionek(aktualny_gracz,DAMA);
	    tab_pol[i][0]=damka;
	  }
    }
}
bool Plansza::czy_wygrana()
{
  int x,y;
    int ilosc_czarnych_pionkow = 0;
    int ilosc_bialych_pionkow = 0;
  for(y=0;y<DL_PLANSZY;y++)
    for(x=0;x<DL_PLANSZY;x++)
      {
	if(tab_pol[x][y].czyj_pion()==CZARNY)
	  ilosc_czarnych_pionkow++;
	if(tab_pol[x][y].czyj_pion()==BIALY)
	  ilosc_bialych_pionkow++;
      }
  if(ilosc_czarnych_pionkow==0)
    {
      cout << "KRZYŻYKI WYGRYWAJĄ" << endl;
      return false;
    }
  if(ilosc_bialych_pionkow==0)
    {
      cout << "BIALE WYGRYWAJĄ" << endl;
      return false;
    }
    return true;
}
int Plansza::ilosc_pionkow_gracza(int gracz)
{
  int x,y;
    int ilosc_czarnych_pionkow = 0;
    int ilosc_bialych_pionkow = 0;
  for(y=0;y<DL_PLANSZY;y++)
    for(x=0;x<DL_PLANSZY;x++)
      {
	if(tab_pol[x][y].czyj_pion()==CZARNY)
	  {
	    if(tab_pol[x][y].jaki_typ()==DAMA)
	      ilosc_czarnych_pionkow=+3;
	    else
	      ilosc_czarnych_pionkow++;
	  }
	if(tab_pol[x][y].czyj_pion()==BIALY)
	  {
	    if(tab_pol[x][y].jaki_typ()==DAMA)
	      ilosc_bialych_pionkow=+3;
	    else
	      ilosc_bialych_pionkow++;
	  }
      }
  if(gracz==CZARNY)
    return ilosc_czarnych_pionkow;
  else
    return ilosc_bialych_pionkow;
}
